import QtQuick 2.12
import Lomiri.Components 1.3

import "../components"
import "../model"

Page {
    id: page

    property PlaylistType playlist

    signal startPlaylist(PlaylistType playlist)
    signal startTrack(var track)
    signal addToEndQueue(var track)
    signal follow(PlaylistType playlist)
    signal unfollow(PlaylistType playlist)

    function refresh(playlistTypeFactory) {
        if (playlist.uuid != '') {
            playlist = playlistTypeFactory.createObject(page, spotSession.getPlaylist(spotSession.loadPlaylist(playlist.uuid)));
        }
    }

    header: PageHeader {
        id: header
        title: playlist.name
        StyleHints {
            dividerColor: LomiriColors.green
        }
    }

    PlaylistView {
        id: playlistView
        playlist: page.playlist
        anchors.top: header.bottom
        height: page.height - header.height
        width: page.width
        onStartPlaylist: page.startPlaylist(playlist)
        onStartTrack: page.startTrack(track)
        onAddToEndQueue: page.addToEndQueue(track)
        onFollow: page.follow(playlist)
        onUnfollow: page.unfollow(playlist)
    }
}