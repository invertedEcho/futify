import QtQuick 2.12
import Lomiri.Components 1.3
import UserMetrics 0.1

import "../components"

Page {
    id: page

    property bool needSave: false

    signal navigate(var pageName, var pageParams)
    signal startTrack(var track)
    signal startPlaylist(var playlist)
    signal startAlbum(var album)
    signal startShow(var show)

    function init() {
        homeYourPlaylists.uuid = '';//force refresh
        homeYourPlaylists.uuid = spotSession.loadUserPlaylists();
        homeYourPlaylists.visible = spotSession.getPlaylists(homeYourPlaylists.uuid).size > 0;
        homeYourAlbums.uuid = '';//force refresh
        homeYourAlbums.uuid = spotSession.loadUserAlbums();
        homeYourAlbums.visible = spotSession.getPlaylists(homeYourAlbums.uuid).size > 0;
        homeSpecialPlaylists.uuid = '';//force refresh
        homeSpecialPlaylists.uuid = spotSession.loadSpecialPlaylists();
        homeSpecialPlaylists.visible = spotSession.getPlaylists(homeSpecialPlaylists.uuid).size > 0;
        homeFeaturedPlaylists.uuid = '';//force refresh
        homeFeaturedPlaylists.uuid = spotSession.loadFeaturedPlaylists();
        homeFeaturedPlaylists.visible = spotSession.getPlaylists(homeFeaturedPlaylists.uuid).size > 0;
        homeYourShows.uuid = '';//force refresh
        homeYourShows.uuid = spotSession.loadUserShows();
        homeYourShows.visible = spotSession.getPlaylists(homeYourShows.uuid).size > 0;
    }

    anchors.fill: parent

    header: PageHeader {
        id: pageHeader
        title: "Futify"

        StyleHints {
            dividerColor: LomiriColors.green
        }

        TextField {
            id: inputSearch
            anchors.top: pageHeader.top
            anchors.right: pageHeader.trailingActionBar.left
            anchors.verticalCenter: pageHeader.verticalCenter
            anchors.topMargin: units.gu(1)
            anchors.bottomMargin: units.gu(1)
            opacity: 0
            onAccepted: {
                if (inputSearch.text !== "") {
                    navigate("search", {"search": inputSearch.text});
                    inputSearch.opacity = 0;
                    inputSearch.text = "";
                } else {
                    animateOpacity.from = 1;
                    animateOpacity.to = 0;
                    animateOpacity.start();
                }

            }
        }
        NumberAnimation {
            id: animateOpacity
            target: inputSearch
            properties: "opacity"
            from: 0.0
            to: 1.0
            duration: 500
            onFinished: {
                if (inputSearch.opacity > 0) {
                    inputSearch.focus = true;
                }
            }
        }

        trailingActionBar {
            numberOfSlots: 2
            actions: [
                Action {
                    text: qsTr("Search")
                    onTriggered: {
                        if (inputSearch.opacity == 0) {
                            animateOpacity.from = 0;
                            animateOpacity.to = 1;
                            animateOpacity.start();
                        } else {
                            animateOpacity.from = 1;
                            animateOpacity.to = 0;
                            animateOpacity.start();
                        }
                    }
                    iconName: "find"
                },
                Action {
                    text: qsTr("Settings")
                    onTriggered: navigate("settings", undefined)
                    iconName: "settings"
                },
                Action {
                    text: qsTr("About")
                    onTriggered: navigate("about", undefined)
                    iconName: "stock_document"
                }
            ]
        }
    }

    Flickable {
        id: home
        anchors.top: pageHeader.bottom
        width: parent.width
        height: parent.height - pageHeader.height
        contentWidth: parent.width
        contentHeight: columnHome.height
        clip: true

        Column {
            id: columnHome
            width: parent.width
            height: units.gu(30) * 6
            topPadding: units.gu(2)
            spacing: units.gu(6)

            HorizontalList {
                id: homeYourPlaylists
                width: parent.width
                visible: false

                title: qsTr("Your playlists")
                icon: 'stock_music'
                uuid: ''
                getSize: function(uuid) {
                    return spotSession.getPlaylists(uuid).size;
                }
                getTotalSize: function(uuid) {
                    return spotSession.getPlaylists(uuid).totalSize;
                }
                getData: function(uuid, index) {
                    return spotSession.getPlaylistByIndex(uuid, index)
                }
                onStart: function(playlist) {
                    startPlaylist(playlist)
                }
                showInfo: true
                onGoToDetails: function(data) {
                    navigate("playlist", data);
                }
                onShowMore: function(uuid) {
                    spotSession.loadMore(uuid);
                    navigate("playlists", {uuid: uuid});
                }
            }

            HorizontalList {
                id: homeYourAlbums
                width: parent.width
                visible: false

                title: qsTr("Your albums")
                icon: 'stock_music'
                uuid: ''
                getSize: function(uuid) {
                    return spotSession.getPlaylists(uuid).size;
                } 
                getTotalSize: function(uuid) {
                    return spotSession.getPlaylists(uuid).totalSize;
                }
                getData: function(uuid, index) {
                    return spotSession.getPlaylistByIndex(uuid, index)
                }
                onStart: function(playlist) {
                    startAlbum(playlist)
                }
                showInfo: true
                onGoToDetails: function(data) {
                    navigate("album", data);
                }
                onShowMore: function(uuid) {
                    spotSession.loadMore(uuid);
                    navigate("playlists", {uuid: uuid});
                }
            }

            HorizontalList {
                id: homeYourShows
                width: parent.width
                visible: false

                title: qsTr("Your shows")
                icon: 'view-list-symbolic'
                uuid: ''
                getSize: function(uuid) {
                    return spotSession.getPlaylists(uuid).size;
                }
                getTotalSize: function(uuid) {
                    return spotSession.getPlaylists(uuid).totalSize;
                }
                getData: function(uuid, index) {
                    return spotSession.getPlaylistByIndex(uuid, index)
                }
                onStart: function(playlist) {
                    startShow(playlist)
                }
                showInfo: true
                onGoToDetails: function(data) {
                    navigate("show", data);
                }
                onShowMore: function(uuid) {
                    spotSession.loadMore(uuid);
                    navigate("playlists", {uuid: uuid});
                }
            }

            HorizontalList {
                id: homeSpecialPlaylists
                width: parent.width
                visible: false

                title: qsTr("Your tracks")
                icon: 'history'
                uuid: ''
                getSize: function(uuid) {
                    return spotSession.getPlaylists(uuid).size;
                }
                getTotalSize: function(uuid) {
                    return spotSession.getPlaylists(uuid).totalSize;
                }
                getData: function(uuid, index) {
                    return spotSession.getPlaylistByIndex(uuid, index)
                }
                onStart: function(playlist) {
                    startPlaylist(playlist)
                }
                showInfo: true
                onGoToDetails: function(data) {
                    navigate("playlist", data);
                }
                onShowMore: function(uuid) {
                    spotSession.loadMore(uuid);
                    navigate("playlists", {uuid: uuid});
                }
            }

            HorizontalList {
                id: homeFeaturedPlaylists
                width: parent.width
                visible: false

                title: qsTr("Featured playlist")
                icon: 'stock_music'
                uuid: ''
                getSize: function(uuid) {
                    return spotSession.getPlaylists(uuid).size;
                }
                getTotalSize: function(uuid) {
                    return spotSession.getPlaylists(uuid).totalSize;
                }
                getData: function(uuid, index) {
                    return spotSession.getPlaylistByIndex(uuid, index)
                }
                onStart: function(playlist) {
                    startPlaylist(playlist)
                }
                showInfo: true
                onGoToDetails: function(data) {
                    navigate("playlist", data);
                }
                onShowMore: function(uuid) {
                    spotSession.loadMore(uuid);
                    navigate("playlists", {uuid: uuid});
                }
            }
        }
    }

}