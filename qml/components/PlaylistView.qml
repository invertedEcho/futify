import QtQuick 2.12
import QtQuick.Controls 2.12
import Lomiri.Components 1.3
import QtGraphicalEffects 1.0

import "../model"

ListView {
    property PlaylistType playlist
    
    signal startPlaylist(PlaylistType playlist)
    signal startTrack(var track)
    signal addToEndQueue(var track)
    signal follow(PlaylistType playlist)
    signal unfollow(PlaylistType playlist)

    clip: true

    header: ListItem {
        height: units.gu(20)
        width: parent.width
        
        Row {
            height: parent.height
            width: parent.width
            padding: units.gu(2)
            spacing: units.gu(2)

            Item {
                id: itemImage
                height: units.gu(16)
                width: itemImage.height
                anchors.verticalCenter: parent.verticalCenter
                Image {
                    id: image
                    source: playlist.image
                    width: parent.width
                    height: parent.height
                    anchors.verticalCenter: parent.verticalCenter
                    fillMode: Image.PreserveAspectFit
                    visible: false
                }
                OpacityMask {
                    anchors.fill: image
                    source: image
                    width: image.width
                    height: image.height
                    maskSource: Rectangle {
                        width: image.width
                        height: image.height
                        radius: 5
                        visible: false // this also needs to be invisible or it will cover up the image
                    }
                }
                // PLAY
                Rectangle {
                    color: "#99FFFFFF"
                    width: units.gu(5)
                    height: units.gu(5)
                    anchors.verticalCenter: parent.verticalCenter
                    anchors.horizontalCenter: parent.horizontalCenter
                    radius: 50
                }
                Item {
                    width: parent.width
                    height: parent.height
                    anchors.top: parent.top
                    TapHandler {
                        onTapped: startPlaylist(playlist)
                    }
                }
                Icon {
                    anchors.verticalCenter: parent.verticalCenter
                    anchors.horizontalCenter: parent.horizontalCenter
                    name: 'media-preview-start'
                    color: "#000000"
                    width: units.gu(5)
                    height: units.gu(5)
                }
            }
            Column {
                anchors.verticalCenter: itemImage.verticalCenter
                Label { text: playlist.name; width: parent.width; elide: Text.ElideLeft; wrapMode: Text.Wrap; maximumLineCount: 1; textSize: Label.Large }
                Label { text: playlist.owner; width: parent.width; elide: Text.ElideLeft; wrapMode: Text.Wrap; maximumLineCount:1; }
                Label { text: playlist.followers + ' ' + qsTr('followers'); textSize: Label.Small; }
                Label { text: playlist.size + ' / ' + playlist.totalSize + ' ' + qsTr('tracks'); textSize: Label.Small; }
            }
            Button {
                visible: playlist.follow !== "followImpossible"
                text: playlist.follow === "follow" ? qsTr("Unfollow") : qsTr("Follow")
                onClicked: {
                    playlist.follow === "follow" ? unfollow(playlist) : follow(playlist)
                }
            }
        }
    }

    model: playlist.size

    ScrollBar.vertical: ScrollBar {
        active: true
    }

    delegate: TrackListItem {
        id: trackListItem
        track: playlist.getTrack(index)
        canDelete: false
        activeSimpleClick: true
        onPlayTrack: {
            console.log('Playlist.qml => start song', trackListItem.track.name)
            startTrack(track);
        }
        onEndQueue: {
            console.log('Playlist.qml => end queue song', trackListItem.track.name)
            addToEndQueue(track);
        }
    }
}