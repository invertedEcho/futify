import QtQuick 2.12
import Lomiri.Components 1.3

Label {
    signal clicked()

    font.italic: false
    font.underline: true

    TapHandler {
        onTapped: {
            clicked();
        }
    }
}