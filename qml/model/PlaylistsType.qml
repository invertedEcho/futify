import QtQuick 2.12

QtObject {
    property string uuid: ""
    property string image: ""
    property string name: ""
    property int totalSize: 0
    property int size: 0

    function getName() {
        if (uuid === "") {
            return ""
        }
        if (uuid === spotSession.loadUserPlaylists()) {
            return qsTr("Your playlists")
        } else if(uuid === spotSession.loadUserAlbums()) {
            return qsTr("Your albums")
        } else if(uuid === spotSession.loadFeaturedPlaylists()) {
            return qsTr("Featured playlist")
        } else if(uuid === spotSession.loadSpecialPlaylists()) {
            return qsTr("Your tracks")
        } else if(uuid === spotSession.loadUserShows()) {
            return qsTr("Your albums")
        }
        return name
    }

    function getTrack(index) {
        return spotSession.getTrackByPlaylist(uuid, index);
    }
}