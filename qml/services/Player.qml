import QtMultimedia 5.12

Audio {
    id: playMusic
    autoLoad: true
    autoPlay: true
    audioRole: MediaPlayer.MusicRole

    signal changes()

    playlist: Playlist {
        id: playerPlaylist
        onItemRemoved: {
            console.log("media removed", start, end, playerPlaylist.currentIndex);
            changes();
        }
        onItemInserted: {
            console.log("media inserted", start, end, playerPlaylist.currentIndex);
            if (currentIndex === -1) {
                currentIndex = 0;
            }
            changes();
        }
        onCurrentIndexChanged: {
            console.log("current index change", playerPlaylist.currentIndex); 
            changes();
        }
    }
    onPlaybackStateChanged: {
        console.log("player state changed", playMusic.playbackState);
        changes();
    }
    onError: {
        console.log("player error", error, errorString);
    }

    function appendAndPlayTrack(track) {
        console.log('appendAndPlayTrack', track.name);
        spotSession.endQueue(track);
        playerPlaylist.addItem(track.path);
        playerPlaylist.currentIndex = spotSession.getAlbum('queue').size - 1;
        if (playMusic.playbackState != Audio.PlayingState) {
            playMusic.playAfterBuffering();
        }
    }

    function appendTrack(track) {
        console.log('appendTrack', track.name);
        spotSession.endQueue(track);
        playerPlaylist.addItem(track.path);
        if (playMusic.playbackState != Audio.PlayingState) {
            playMusic.playAfterBuffering();
        }
    }

    function startPlaylist(playlist) {
        spotSession.loadPlaylist(playlist.uuid);
        var playlistLoaded = spotSession.getPlaylist(playlist.uuid);
        var data = [];
        spotSession.clearQueue();
        for (var i=0; i<playlistLoaded.size-1; i++) {
            var track = playlist.getTrack(i);
            data.push(track.path);
            spotSession.endQueue(track);
        }
        playerPlaylist.removeItems(0, playerPlaylist.itemCount - 1);
        playerPlaylist.addItems(data);
        if (playMusic.playbackState != Audio.PlayingState) {
            playMusic.playAfterBuffering();
        }
    }

    function startAlbum(album) {
        spotSession.loadAlbum(album.uuid);
        var albumLoaded = spotSession.getAlbum(album.uuid);
        var data = [];
        spotSession.clearQueue();
        for (var i=0; i<albumLoaded.size-1; i++) {
            var track = album.getTrack(i);
            data.push(track.path);
            spotSession.endQueue(track);
        }
        playerPlaylist.removeItems(0, playerPlaylist.itemCount - 1);
        playerPlaylist.addItems(data);
        if (playMusic.playbackState != Audio.PlayingState) {
            playMusic.playAfterBuffering();
        }
    }

    function startShow(show) {
        spotSession.loadShow(show.uuid);
        var showLoaded = spotSession.getShow(show.uuid);
        var data = [];
        spotSession.clearQueue();
        for (var i=0; i<showLoaded.size-1; i++) {
            var track = show.getTrack(i);
            data.push(track.path);
            spotSession.endQueue(track);
        }
        playerPlaylist.removeItems(0, playerPlaylist.itemCount - 1);
        playerPlaylist.addItems(data);
        if (playMusic.playbackState != Audio.PlayingState) {
            playMusic.playAfterBuffering();
        }
    }

    function clear() {
        playMusic.stop();
        spotSession.clearQueue();
        playerPlaylist.removeItems(0, playerPlaylist.itemCount - 1);
    }

    function newTimer() {
        return Qt.createQmlObject("import QtQuick 2.0; Timer {}", root);
    }

    function playAfterBuffering() {
        var timer = newTimer();
        timer.interval = 100;
        timer.repeat = true;
        timer.triggered.connect(function () {
            if(playMusic.playbackState == Audio.PlayingState) {
                timer.stop();
            } else if(playMusic.bufferProgress >= 0.3 && playMusic.playbackState != Audio.PlayingState) {
                playMusic.play();
            }
        })
        timer.start();
    }

}