package server

import (
	"bufio"
	"encoding/json"
	"errors"
	"fmt"
	"io/ioutil"
	"os"
	"path"
	"regexp"

	"github.com/librespot-org/librespot-golang/librespot/core"
	"github.com/nanu-c/qml-go"
	"github.com/zmb3/spotify/v2"
)

type Settings struct {
	Quality   int //0 == low, 1 == medium, 2 == high
	Theme     int //0 == dark, 1 == light, 2 == system
	Lang      int //0 == system, 1 == ...
	ErrorSong int // 0 == silence, 1 == error
}

type Contributors struct {
	Name      string
	AvatarUrl string
	Link      string
}

type Session struct {
	Root          qml.Object
	spotSession   *core.Session
	spotAPIClient *spotify.Client
	IsLogged      bool
	Username      string
	Settings      *Settings
	playlists     map[string]*Playlists
	playlist      map[string]*Playlist
	albums        map[string]*Album
	show          map[string]*Show
	searchResults *SearchResults
	tracks        map[string]*Track
	contributors  []*Contributors
	port          int
	engine        *qml.Engine
}

func NewSession(port int, engine *qml.Engine) *Session {
	spotSession := &Session{
		IsLogged: false,
		Username: "",
		Settings: &Settings{
			Quality: 1,
			Theme:   0,
			Lang:    0,
		},
		albums:       make(map[string]*Album),
		playlists:    make(map[string]*Playlists),
		playlist:     make(map[string]*Playlist),
		tracks:       make(map[string]*Track),
		show:         make(map[string]*Show),
		contributors: loadContributors(),
		port:         port,
		engine:       engine,
	}
	spotSession.albums["queue"] = &Album{Size: 0, Name: "Queue", Uuid: "queue", tracks: make([]*Track, 0)}
	spotSession.loadConfig()
	return spotSession
}

func (s *Session) locale() string {
	switch s.Settings.Lang {
	case 0:
		return ""
	case 1:
		return "de_DE"
	case 2:
		return "en_EN"
	case 3:
		return "es_ES"
	case 4:
		return "fr_FR"
	case 5:
		return "hu_HU"
	case 6:
		return "nl_NL"
	case 7:
		return "sv_SE"
	}
	fmt.Println(fmt.Sprintf("Unknown locale %d", s.Settings.Lang))
	return ""
}

func (s *Session) loadConfig() error {
	data, err := ioutil.ReadFile(path.Join(configPath, "settings.json"))
	if err != nil {
		if errors.Is(err, os.ErrNotExist) {
			return nil
		}
		return err
	}
	fmt.Println("Settings exist!")
	if err := json.Unmarshal(data, s.Settings); err != nil {
		fmt.Println("Bad formatted settings")
		return err
	}

	return nil
}

func loadContributors() []*Contributors {
	contributors := make([]*Contributors, 0)

	file, err := os.Open("CONTRIBUTORS.md")
	if err != nil {
		fmt.Println(err)
		return contributors
	}
	defer file.Close()

	scanner := bufio.NewScanner(file)
	var txt string
	r := regexp.MustCompile("^\\|\\s*!\\[[^\\]]+\\]\\(([^\\]]+)\\)\\s*\\|\\s*([^\\s]+)\\s*\\|\\s*\\[[^\\]]+\\]\\(([^\\)]+)")
	var res []string
	for scanner.Scan() {
		txt = scanner.Text()
		res = r.FindStringSubmatch(txt)
		if len(res) > 1 {
			contributors = append(contributors, &Contributors{Name: res[2], AvatarUrl: res[1], Link: res[3]})
		}
	}
	return contributors
}
