package server

import (
	"fmt"
	"io"
	"time"

	"github.com/librespot-org/librespot-golang/Spotify"
	"github.com/librespot-org/librespot-golang/librespot/player"
)

type FutifyAudioFile struct {
	track       *Track
	audioFile   *player.AudioFile
	spotifyFile *Spotify.AudioFile
}

func NewFutifyAudioFile(track *Track, audioFile *player.AudioFile, spotifyFile *Spotify.AudioFile) *FutifyAudioFile {
	a := &FutifyAudioFile{track: track, audioFile: audioFile, spotifyFile: spotifyFile}
	return a
}

func (file *FutifyAudioFile) WaitForSize() error {
	buf := make([]byte, 32*1024)
	totalDownload := 0
	initAudioSize := file.audioFile.Size()
	fmt.Println("start with", initAudioSize, "size")
	for {
		size, err := file.audioFile.Read(buf)
		totalDownload += size
		if file.audioFile.Size() > initAudioSize || totalDownload >= 32*1024*10 {
			fmt.Println("finish with", file.audioFile.Size(), "size and", totalDownload, "bytes")
			file.audioFile.Seek(0, io.SeekStart)
			return nil
		}
		if err != nil {
			file.audioFile.Seek(0, io.SeekStart)
			return err
		}
		if size == 0 {
			time.Sleep(100 * time.Millisecond)
		}
	}
}

func (file *FutifyAudioFile) Read(p []byte) (int, error) {
	for {
		size, err := file.audioFile.Read(p)
		if size == 0 && err == nil {
			time.Sleep(100 * time.Millisecond)
			continue
		}
		//fmt.Println("read", size, err)
		return size, err
	}
}

func (file *FutifyAudioFile) Seek(offset int64, whence int) (int64, error) {
	size, err := file.audioFile.Seek(offset, whence)
	//fmt.Println("seek", size, err)
	return size, err
}
